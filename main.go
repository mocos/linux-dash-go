package main

import (
	"github.com/astaxie/beego"
	"linux-dash-go/controllers"
)

func main() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/issue", &controllers.MainController{}, "*:Issue")
	beego.Router("/hostname", &controllers.MainController{}, "*:Hostname")
	beego.Router("/uptime", &controllers.MainController{}, "*:Uptime")
	beego.Router("/time", &controllers.MainController{}, "*:Time")

	beego.Run()
}
