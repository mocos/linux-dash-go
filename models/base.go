package models

import (
	"bytes"
	"os/exec"
)

func Cmd(s string) string {
	cmd := exec.Command("/bin/sh", "-c", s)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return ""
	}

	str := out.String()
	n := len(str)

	return str[:n-1]
}
