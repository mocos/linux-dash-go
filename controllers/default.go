package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"linux-dash-go/models"
	"strconv"
)

type MainController struct {
	beego.Controller
}

func (this *MainController) Get() {
	this.TplNames = "index.html"
}

func (this *MainController) Issue() {
	this.Data["json"] = models.Cmd("/usr/bin/lsb_release -ds;/bin/uname -r")
	this.ServeJson()
}

func (this *MainController) Hostname() {
	this.Data["json"] = models.Cmd("/bin/hostname")
	this.ServeJson()
}

func (this *MainController) Time() {
	this.Data["json"] = models.Cmd("/bin/date")
	this.ServeJson()
}

func (this *MainController) Uptime() {

	totalSecondsStr := models.Cmd("/usr/bin/cut -d. -f1 /proc/uptime")

	totalSeconds, err := strconv.Atoi(totalSecondsStr)
	if err != nil {
		fmt.Println(err)
		totalSeconds = 1
	}

	totalMin := totalSeconds / 60
	totalHours := totalMin / 60

	days := totalHours / 24
	hours := totalHours - (days * 24)
	min := totalMin - (days * 60 * 24) - (hours * 60)

	formatUptime := ""
	if days != 0 {
		formatUptime += fmt.Sprintf("%v days ", days)
	}

	if hours != 0 {
		formatUptime += fmt.Sprintf("%v hours ", hours)
	}

	if min != 0 {
		formatUptime += fmt.Sprintf("%v minutes ", min)
	}

	this.Data["json"] = formatUptime
	this.ServeJson()
}
